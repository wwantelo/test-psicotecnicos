
# Archivos css adicionales
Rails.application.config.assets.precompile += %w( metisMenu.min.css )
Rails.application.config.assets.precompile += %w( timeline.css )
Rails.application.config.assets.precompile += %w( sb-admin-2.css )
Rails.application.config.assets.precompile += %w( morris.css )
Rails.application.config.assets.precompile += %w( jquery-ui.min.css )

# Archivos js adicionales
Rails.application.config.assets.precompile += %w( jquery.js )
Rails.application.config.assets.precompile += %w( jquery-ui.min.js )
Rails.application.config.assets.precompile += %w( bootstrap.min.js )
Rails.application.config.assets.precompile += %w( metisMenu.js )
Rails.application.config.assets.precompile += %w( raphael.min.js )
Rails.application.config.assets.precompile += %w( morris.min.js )
Rails.application.config.assets.precompile += %w( sb-admin-2.js )